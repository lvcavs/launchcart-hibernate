package org.launchcode.launchcart.models;


import javax.persistence.MappedSuperclass;
import java.util.Random;

/**
 * Created by LaunchCode
 */
@MappedSuperclass
public class AbstractEntity {

    private int uid;

    public int getUid() {
        return uid;
    }

    AbstractEntity() {
        this.uid = new Random().nextInt();
    }
}
