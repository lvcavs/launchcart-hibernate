package org.launchcode.launchcart.models;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by LaunchCode
 */

public class Cart extends AbstractEntity {

    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) { items.remove(item); }

    public double computeTotal() {
        double total = 0.0;
        for(Item item : getItems()) {
            total += item.getPrice();
        }
        return total;
    }

}
