package org.launchcode.launchcart.data;

import org.junit.Before;
import org.junit.Test;
import org.launchcode.launchcart.models.Item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by LaunchCode
 */
public class ItemMemoryRepositoryTests {

    private ItemMemoryRepository itemMemoryRepository;

    @Before
    public void setupCart() {
       itemMemoryRepository = new ItemMemoryRepository();
       itemMemoryRepository.clear();
    }

    //TODO: test that ItemMemoryRepository can...
    // 1) save multiple items
    // 2) each item has a unique uid
    // 3) you can find the item by it's uid
    @Test
    public void testAddItems() {
        Item item1 = new Item("Test Item 1", 15.11);
        Item item2 = new Item("Test Item 2", 12.22);
        Item item3 = new Item("Test Item 3", 44.67);
        itemMemoryRepository.save(item1);
        itemMemoryRepository.save(item2);
        itemMemoryRepository.save(item3);
        assertEquals(itemMemoryRepository.findOne(item1.getUid()).getName(), "Test Item 1");
        assertEquals(itemMemoryRepository.findOne(item1.getUid()).getPrice(), 15.11, .001);
        assertEquals(itemMemoryRepository.findOne(item2.getUid()).getName(), "Test Item 2");
        assertEquals(itemMemoryRepository.findOne(item2.getUid()).getPrice(), 12.22, .001);
        assertEquals(itemMemoryRepository.findOne(item3.getUid()).getName(), "Test Item 3");
        assertEquals(itemMemoryRepository.findOne(item3.getUid()).getPrice(), 44.67, .001);
    }
}
